#| -*- lisp -*-
exec sbcl --script $0 $* # |#
(load "mlib")
(load "tio")
(λ:>>(a b) = (format t "~6d~c~x~%" a #\tab b))
(⭅ *posix-argv*)
(∞ (:=* ((filename (⭅ *posix-argv*))
	 (line-no 0))
	(when (null filename) (return))
	(with-open-file(s filename)
	  (ε (∞ (:>> (++ line-no) (read-line s)))
	     (end-of-file() t)))))
