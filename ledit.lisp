(require :sb-posix)
(defpackage :ledit (:use :cl :sb-posix)
	    (:shadow time abort open close
		     read write ftruncate truncate)
	    (:import-from sb-sys *tty*))
(in-package :ledit)

(load "macros")
(load "tio")

(raw t)

(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))
(format t "[~s]~%" (read-char))

