(defmacro andc2(a c)`(setf,a(logandc2,a,c)))
(defmacro add-exit-hook(fn)`(push,fn sb-ext:*exit-hooks*))
(defmacro λ(name args &rest rest)`(defun,name,args,@rest))
(defmacro λλ(args &rest rest)`(lambda,args,@rest))
(defmacro λλλ(    &rest rest)`(lambda ()  ,@rest))
(set-macro-character #\… (λλ(s c) (declare (ignore s c)) '&rest))
(λ qqqq(x … y)
;;(defun qqqq(x &rest y)
  (format t "x ~S~%" x)
  (format t "y ~S~%" y)
  t)
(format t "~S~%" (qqqq 1 2 3 4))
